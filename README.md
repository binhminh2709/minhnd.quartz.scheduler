# minhnd.quartz.scheduler
Quartz scheduler to help Java application to scheduler a job/task to run at a specified date and time.

Note To deploy Quartz on Application server like JBoss, oracle or weblogic, you may need addition Quartz dependency, read this: http://quartz-scheduler.org/downloads/catalog

Quartz trigger is defined when the Quartz will run your above Quartz’s job
 
There are 2 types of triggers in Quartz 2
SimpleTrigger – Allows to set start time, end time, repeat interval.
CronTrigger – Allows Unix cron expression to specify the dates and times to run your job.

